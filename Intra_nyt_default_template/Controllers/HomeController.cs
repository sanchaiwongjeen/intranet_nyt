﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Intra_nyt_default_template.Models;
using Intra_nyt.Interface;
using Microsoft.AspNetCore.Authentication;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Hosting;

namespace Intra_nyt_default_template.Controllers
{
    public class HomeController : Controller
    {
        public readonly Intra_nytContext _context;
        public readonly IAuthen _Authen;
        public readonly IHostingEnvironment _env;
        public HomeController(Intra_nytContext context,IAuthen authen,IHostingEnvironment env)
        {
            _context = context;
            _Authen = authen;
            _env = env;
        }
        public async Task<IActionResult> Index_intra(string frompage) {
            var data = _context.nyt_News
                     .Where(n => n.status == "Main content")
                     .OrderByDescending(n => n.Id)
                     .Take(1);
            ViewBag.dataimg = data.ToList();

            var datanormal = _context.nyt_News
                .OrderByDescending(n => n.Id)
                .Take(6);
            ViewBag.datanormal = datanormal.ToList();
            if (frompage != null)
            {
                String Id = await _Authen.sentToken("emp_Id");
                String emp_name = await _Authen.sentToken("emp_name");
                String emp_sname = await _Authen.sentToken("emp_sname");
                if ((Id == "") || (emp_name == "") || (emp_sname == ""))
                {
                    return View();
                }
                else
                {
                    var UserRole = _context.authori
                              .Where(e => e.emp_Id.Trim() == Id && e.project_name == "news")
                              .ToList();

                    var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name,emp_name+" "+emp_sname),
                new Claim("employeeId",Id)
            };
                    //add userRole
                    foreach (var item in UserRole)
                    {
                        claims.Add(new Claim(ClaimTypes.Role, item.autho));
                    }

                    ClaimsIdentity userIdentity = new ClaimsIdentity(claims, "LvCookie");
                    ClaimsPrincipal principal = new ClaimsPrincipal(userIdentity);

                    //await _httpContextAccessor.HttpContext.SignInAsync(principal);
                    await HttpContext.SignInAsync(
                        CookieAuthenticationDefaults.AuthenticationScheme,
                         principal,
                      new AuthenticationProperties
                      {
                          ExpiresUtc = DateTime.UtcNow.AddHours(1),
                          IsPersistent = false,
                          AllowRefresh = false
                      });
                    return RedirectToAction("Index_intra", "Home");
                }
            }

            return View();
        }
        //[Route("/{fromlogin}")]
        public async Task<IActionResult> Index(String frompage)
        {
           // await HttpContext.SignOutAsync();
            var data = _context.nyt_News
                      .Where(n => n.status == "Main content")
                      .OrderByDescending(n => n.Id)
                      .Take(3);
            ViewBag.dataimg = data.ToList();
            
            var datanormal = _context.nyt_News
                .OrderByDescending(n => n.Id)
                .Take(6);
            ViewBag.datanormal = datanormal.ToList();
            if(frompage != null)
            {
                String Id = await _Authen.sentToken("emp_Id");
                String emp_name = await _Authen.sentToken("emp_name");
                String emp_sname = await _Authen.sentToken("emp_sname");
                if ((Id == "") || (emp_name == "") || (emp_sname == ""))
                {
                    return View();
                }
                else
                {
                    var UserRole = _context.authori
                              .Where(e => e.emp_Id.Trim() == Id && e.project_name == "news")
                              .ToList();

                    var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name,emp_name+" "+emp_sname),
                new Claim("employeeId",Id)
            };
                    //add userRole
                    foreach (var item in UserRole)
                    {
                        claims.Add(new Claim(ClaimTypes.Role, item.autho));
                    }

                    ClaimsIdentity userIdentity = new ClaimsIdentity(claims, "LvCookie");
                    ClaimsPrincipal principal = new ClaimsPrincipal(userIdentity);

                    //await _httpContextAccessor.HttpContext.SignInAsync(principal);
                    await HttpContext.SignInAsync(
                        CookieAuthenticationDefaults.AuthenticationScheme,
                         principal,
                      new AuthenticationProperties
                      {
                          ExpiresUtc = DateTime.UtcNow.AddHours(1),
                          IsPersistent = false,
                          AllowRefresh = false
                      });
                    return RedirectToAction("Index", "Home");
                }
            }

            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }
        //[Route("edit/{name}")]
        public IActionResult Contact(String name)
        {
            ViewData["Message"] = "Your contact page."+name;

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
