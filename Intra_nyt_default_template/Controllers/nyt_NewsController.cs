﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Intra_nyt_default_template.Models;
using nyt_Netcore.Models;
using Intra_nyt.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using ReflectionIT.Mvc.Paging;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Routing;

namespace Intra_nyt_default_template.Controllers
{
    public class emp
    {
        string emp_name { get; set; }
    }
    public class nyt_NewsController : Controller
    {
        private readonly Intra_nytContext _context;
        private readonly IsaveFile _IsaveFile;
        private readonly IAuthen _Authen;
        public String _name = "";
       
        public nyt_NewsController(Intra_nytContext context, IsaveFile IsaveFile, IAuthen authen)
        {
            _context = context;
            _IsaveFile = IsaveFile;
            _Authen = authen;
        }
        public async Task<IActionResult> Index(String status,String dateFrom,String dateTo,int page = 1)
        {

            var news = from n in _context.nyt_News
                       select n;


            if (!string.IsNullOrEmpty(status))
            {
                news = news.Where(s => s.status.Contains(status));
            }

            if (!string.IsNullOrEmpty(dateFrom) && !string.IsNullOrEmpty(dateTo))
            {
                String[] fromArr = dateFrom.Split('/');
                String[] toArr = dateTo.Split('/');
                String fromDay = fromArr[2] + "-" + fromArr[1] + "-" + fromArr[0];
                String toDay = toArr[2] + "-" + toArr[1] + "-" + toArr[0];
                news = news.Where(s => s.createdate >= DateTime.Parse(fromDay) && s.createdate <= DateTime.Parse(toDay));
            }


            news = news.OrderBy(n => n.status);

            var model = await PagingList.CreateAsync(
                                   news, 1, page, "status", "status");

            model.RouteValue = new RouteValueDictionary
            {
                {"dateFrom",dateFrom },
                {"dateTo",dateTo },
                {"status",status }
            };

            ViewBag.status = status;
           
            return View(model);
            //return View(await _context.nyt_News.ToListAsync());
        }
        public async Task<IActionResult> index_intra(String status, String dateFrom, String dateTo, int page = 1)
        {
            var news = from n in _context.nyt_News
                       select n;
            if (!string.IsNullOrEmpty(status))
            {
                news = news.Where(s => s.status.Contains(status));
            }

            if (!string.IsNullOrEmpty(dateFrom) && !string.IsNullOrEmpty(dateTo))
            {
                String[] fromArr = dateFrom.Split('/');
                String[] toArr = dateTo.Split('/');
                String fromDay = fromArr[2] + "-" + fromArr[1] + "-" + fromArr[0];
                String toDay = toArr[2] + "-" + toArr[1] + "-" + toArr[0];
                news = news.Where(s => s.createdate >= DateTime.Parse(fromDay) && s.createdate <= DateTime.Parse(toDay));
            }


            news = news.OrderBy(n => n.status);

            var model = await PagingList.CreateAsync(
                                   news, 1, page, "status", "status");

            model.RouteValue = new RouteValueDictionary
            {
                {"dateFrom",dateFrom },
                {"dateTo",dateTo },
                {"status",status }
            };

            ViewBag.status = status;

            return View(model);
        }

        // GET: nyt_News/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            //getAuthen();
            if (id == null)
            {
                return NotFound();
            }

            var nyt_News = await _context.nyt_News
                .FirstOrDefaultAsync(m => m.Id == id);
            if (nyt_News == null)
            {
                return NotFound();
            }

            var imgDetail = _context.gallery_News
                           .Where(n => n.ref_Id == id.ToString())
                           .OrderBy(n => n.Id);
            ViewBag.gallery = imgDetail.ToList();
            return View(nyt_News);
        }

        // GET: nyt_News/Create
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> Create()
        {

            IQueryable<string> depQu = from m in _context.department
                        orderby m.dep_name
                        select m.dep_name;
            var news = new nyt_News
            {
                deplist = new SelectList(await depQu.Distinct().ToListAsync()),
                //employees = await emp.ToListAsync()
            };
            return View(news);
        }

        // POST: nyt_News/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public async Task<IActionResult> Create(nyt_News nyt_News,List<IFormFile> img_hidden0,gallery_News gallery_News,IFormFile file_hidden0)
        {
            if (ModelState.IsValid)
            {
                for (int i = 0; i < img_hidden0.Count; i++)
                {
                    String path = "";
                    String file = "";
                    if (i == 0)
                    {
                        if (img_hidden0[0] != null)
                        {
                            path = "/images/News/";
                            file = await _IsaveFile.save(img_hidden0[0], path);
                            nyt_News.img = file;
                            nyt_News.attachfile = await _IsaveFile.save(file_hidden0, "/attachfile/News/");
                            nyt_News.createdate = DateTime.Now;
                            _context.Add(nyt_News);
                            await _context.SaveChangesAsync();
                        }
                    }
                    if (img_hidden0[i] != null)
                    {
                        if (img_hidden0[i] != null)
                        {
                            path = "/images/News/";
                            file = await _IsaveFile.save(img_hidden0[i], path);
                            var gall = new gallery_News {
                                ref_Id = nyt_News.Id.ToString(),
                                type = "Image",
                                path=file,
                            };
                            _context.gallery_News.Add(gall);
                        }
                    }

                }
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(nyt_News);
        }

        // GET: nyt_News/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            //getAuthen();
            if (id == null)
            {
                return NotFound();
            }

            var nyt_News = await _context.nyt_News.FindAsync(id);
            if (nyt_News == null)
            {
                return NotFound();
            }
            return View(nyt_News);
        }

        // POST: nyt_News/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,title,detail,img,attachfile,createdate,status,department")] nyt_News nyt_News)
        {
            if (id != nyt_News.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(nyt_News);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!nyt_NewsExists(nyt_News.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(nyt_News);
        }

        // GET: nyt_News/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var nyt_News = await _context.nyt_News
                .FirstOrDefaultAsync(m => m.Id == id);
            if (nyt_News == null)
            {
                return NotFound();
            }

            return View(nyt_News);
        }

        // POST: nyt_News/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var nyt_News = await _context.nyt_News.FindAsync(id);
            _context.nyt_News.Remove(nyt_News);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool nyt_NewsExists(int id)
        {
            return _context.nyt_News.Any(e => e.Id == id);
        }
    }
}
