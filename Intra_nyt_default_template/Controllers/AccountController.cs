﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Intra_nyt.Interface;
using Intra_nyt_default_template.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Intra_nyt_default_template.Controllers
{
    public class AccountController : Controller
    {
        private readonly Intra_nytContext _context;
        private IHttpContextAccessor _httpContextAccessor;
        public AccountController(Intra_nytContext context, IHttpContextAccessor httpCon)
        {
            _context = context;
            _httpContextAccessor = httpCon;
        }

        public IActionResult AccessDenied()
        {
             return View();
        }

        public IActionResult Login()
        {
            return Redirect("https://localhost:44304/");
           // return View();
        }
        [Route("Account/Logout")]
        public async Task<IActionResult> Logout()
        {
            Response.Cookies.Delete("AuthenApplication");
            await HttpContext.SignOutAsync();

            //return Redirect("https://localhost:44304/");
            return RedirectToAction("Index", "Home");
        }
        [HttpPost]
        //public async Task<IActionResult> Login(String user,String pass)
        //{
        //    IActionResult response = Unauthorized();
        //    var emp = _context.Employee
        //              .Where(e => e.emp_Id.Trim() == user && e.pass == pass)
        //              .FirstOrDefault();
        //    if (emp != null)
        //    {
        //        var tokenString = _BuidToken.BuidToken(user, pass);
        //        response = Ok(new { token = tokenString });
        //        //return RedirectToAction("index", "nyt_News");
        //        return Redirect("http://localhost:1150");


        //    }
            
        //    return response;
        //}
        [Authorize(Roles ="user")]
        public IEnumerable<Employee> get()
        {
            IEnumerable<Employee> _data = _context.Employee.ToList();

            return _data;
        }
    }
}