﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Intra_nyt_default_template.Models;
using Microsoft.AspNetCore.Mvc;

namespace Intra_nyt_default_template.Controllers
{
    public class DocumentController : Controller
    {
        private readonly Intra_nytContext _context;
        public DocumentController(Intra_nytContext context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            DocumentViewModels doc = new DocumentViewModels();
            doc.DocumentList = _context.documents.ToList();
            return View(doc);
        }

        public async Task<IActionResult> GetDocument()
        {
            try
            {
                return Ok();
            }
            catch(Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
            
        }
    }
}