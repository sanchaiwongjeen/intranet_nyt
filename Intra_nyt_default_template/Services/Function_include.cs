﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Session;
using Microsoft.AspNetCore.Http;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Intra_nyt.Interface;
using System.IO;
using System.Drawing;
using ImageMagick;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using Microsoft.AspNetCore.Mvc;
using static System.Net.Mime.MediaTypeNames;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.Text.Encodings;
using Microsoft.Extensions.Configuration;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Intra_nyt_default_template.Models;
using System.Web;
namespace Login_Api.Functions
{
    public class Function_include
    {
        const String conString = "Server=NB-IT01\\SQLEXPRESS;Database=intra_NYT;Trusted_Connection=True;MultipleActiveResultSets=true";


        public bool Authori(String authori)
        {
            
            SqlConnection con = new SqlConnection();
            try
            {
                
                string SQL = "select * from dbo.authori where autho = '"+authori.Trim()+"'";
                SqlCommand cmd = new SqlCommand(SQL, con);
                con.Open();
                SqlDataReader Reader = cmd.ExecuteReader();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace.ToString());
            }
            finally
            {
                con.Close();
            }
            return true;

        }
    }

   public class Hashing : IHashing
    {
        public String HashPass(String pass)
        {
            try
            {
                Console.Write("Enter a password: ");
                string password = "sanvhai";
                byte[] salt = new byte[128 / 8];
                Console.WriteLine($"Salt: {Convert.ToBase64String(salt)}");
                string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                    password: password,
                    salt: salt,
                    prf: KeyDerivationPrf.HMACSHA1,
                    iterationCount: 10000,
                    numBytesRequested: 256 / 8));
                Console.WriteLine($"Hashed: {hashed}");
                return (hashed);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.StackTrace.ToString());
                return(null);
            }
           
        }
    }

    public class saveFile:IsaveFile
    {
       
        public async Task<String> save(IFormFile fileName, String filePath)
        {
            try
            {
                String n = fileName.ToString();
                string path = filePath;
                string pathSave = $"wwwroot{path}";
                if (Directory.Exists(pathSave))
                {
                    Directory.CreateDirectory(pathSave);
                }
                string extFile = Path.GetExtension(fileName.FileName);
                string nameFile = Path.GetFileName(fileName.FileName);
                string file_Name = DateTime.Now.ToString("ddMMyyyyhhmmss") + nameFile;
                var path_ = Path.Combine(Directory.GetCurrentDirectory(), pathSave, file_Name);
              
                String result = path + file_Name;
                if (extFile.Equals(".jpg") || extFile.Equals(".png") || extFile.Equals(".gif"))
                {
                    using (MagickImage image = new MagickImage(fileName.OpenReadStream()))
                    {
                        MagickGeometry size = new MagickGeometry(1140, 360);
                        size.IgnoreAspectRatio = true;
                        image.Resize(size);
                        image.Write(path_);
                    }
                }
                else
                {
                    using (var stream = new FileStream(path_, FileMode.Create))
                    {
                        await fileName.CopyToAsync(stream);
                    }
                }


                return (result);
            }
           catch(Exception ex)
            {
                Console.WriteLine(ex.StackTrace.ToString());
                return (null);
            }
        }




    }

    public class TokenJWT:IBuidToken
    {

        private IConfiguration Config { get; }
        private readonly Intra_nytContext _context;
        public TokenJWT(IConfiguration config,Intra_nytContext context)
        {
            Config = config;
            _context = context;
        }
        public string BuidToken(String user, String pass)
        {

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Config["Jwt:Key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddMinutes(Convert.ToDouble(Config["Jwt:Expires"]));

            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub,user),
                new Claim("sanchai","namyongterminal"),
                new Claim("Roles","user"),
                new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString())
              
            };

       
            var token = new JwtSecurityToken(
                Config["Jwt:Issuer"],
                Config["Jwt:Issuer"],
                claims,
                expires: expires,
                signingCredentials: creds
                );
            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }

     public class authenbyToken : IAuthen
    {
        private readonly Intra_nytContext _context;
        public IHttpContextAccessor _httpContextAccessor;
        // private readonly IHttpContextAccessor _httpContextAccessor;
        public authenbyToken(Intra_nytContext context,IHttpContextAccessor httpCon)
        {
            _context = context;
            _httpContextAccessor = httpCon;
        }
        public async Task<String> sentToken(String type)
        {
            //String token = "";
            String token = _httpContextAccessor.HttpContext.Request.Cookies["AuthenApplication"];
            String Id = "";
            String name = "";
            String sname = "";
            String value = "";
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            string url = "http://192.168.2.11/account/Account/get?token=" + token;
            var response = await client.GetAsync(url);
            string jsonResult = await response.Content.ReadAsStringAsync();
            if(jsonResult != "")
            {
                // await _httpContextAccessor.HttpContext.SignOutAsync();
                // Id = getResult(jsonResult,"emp_Id");
                //name = getResult(jsonResult, "emp_name");
                //sname = getResult(jsonResult, "emp_sname");
                //setClaim(Id,name,sname);
                value = getResult(jsonResult,type);
            }
            return value;

        }

        public String getResult(String json,String result)
        {
            string name = "";
            String value = "";
            JArray a = JArray.Parse(json);
            foreach (JObject o in a.Children<JObject>())
            {
                foreach (JProperty p in o.Properties())
                {
                    name = p.Name;
                    if (name.Equals(result))
                    {
                        value = (string)p.Value;
                    }

                    Console.WriteLine(name + " -- " + value);
                }
            }
            return value;
        }

        public async Task setClaim(String Id,String name,String sname)
        {
          
            // IEnumerable<authori> UserRole = getAuthori(Id);
            var UserRole = _context.authori
                           .Where(e => e.emp_Id.Trim() == Id)
                           .ToList();

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name,Id),
                new Claim(ClaimTypes.Surname,sname)
            };

            //add userRole
            foreach (var item in UserRole)
            {
                claims.Add(new Claim(ClaimTypes.Role, item.autho));
            }
            ClaimsIdentity userIdentity = new ClaimsIdentity(claims, "LvCookie");
            ClaimsPrincipal principal = new ClaimsPrincipal(userIdentity);

            //await _httpContextAccessor.HttpContext.SignInAsync(principal);
            await _httpContextAccessor.HttpContext.SignInAsync(
                CookieAuthenticationDefaults.AuthenticationScheme,
                 principal,
              new AuthenticationProperties
              {
                  ExpiresUtc = DateTime.UtcNow.AddHours(1),
                  IsPersistent = false,
                  AllowRefresh = false
              });
        }
       

    }

  

}
