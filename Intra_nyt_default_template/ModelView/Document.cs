﻿using Intra_nyt_default_template.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Intra_nyt_default_template.ModelsView
{
    public class Document
    {
        public int Id { get; set; }
        public string title { get; set; }
        public string path { get; set; }
        public DateTime createdate { get; set; }
        public string createby { get; set; }
        public string dept { get; set; }
        
    }
}
