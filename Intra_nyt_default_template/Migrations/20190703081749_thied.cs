﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Intra_nyt_default_template.Migrations
{
    public partial class thied : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "test",
                table: "department");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "test",
                table: "department",
                nullable: true);
        }
    }
}
