﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Intra_nyt_default_template.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.CreateTable(
            //    name: "department",
            //    columns: table => new
            //    {
            //        Id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
            //        dep_Id = table.Column<string>(nullable: true),
            //        dep_name = table.Column<string>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_department", x => x.Id);
            //    });

            migrationBuilder.CreateTable(
                name: "gallery_News",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ref_Id = table.Column<string>(nullable: true),
                    type = table.Column<string>(nullable: true),
                    path = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_gallery_News", x => x.Id);
                });

            //migrationBuilder.CreateTable(
            //    name: "nyt_News",
            //    columns: table => new
            //    {
            //        Id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
            //        title = table.Column<string>(maxLength: 20, nullable: false),
            //        detail = table.Column<string>(maxLength: 1000, nullable: false),
            //        img = table.Column<string>(maxLength: 100, nullable: false),
            //        attachfile = table.Column<string>(maxLength: 100, nullable: false),
            //        createdate = table.Column<DateTime>(nullable: true),
            //        status = table.Column<string>(nullable: true),
            //        department = table.Column<string>(nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_nyt_News", x => x.Id);
            //    });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropTable(
            //    name: "department");

            migrationBuilder.DropTable(
                name: "gallery_News");

            //migrationBuilder.DropTable(
            //    name: "nyt_News");
        }
    }
}
