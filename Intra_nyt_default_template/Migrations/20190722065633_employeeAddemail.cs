﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Intra_nyt_default_template.Migrations
{
    public partial class employeeAddemail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "authori",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    emp_Id = table.Column<string>(nullable: true),
                    project_name = table.Column<string>(nullable: true),
                    autho = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_authori", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Employee",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    emp_Id = table.Column<string>(nullable: true),
                    dep_Id = table.Column<string>(nullable: true),
                    emp_name = table.Column<string>(nullable: true),
                    emp_sname = table.Column<string>(nullable: true),
                    emp_picture = table.Column<string>(nullable: true),
                    address = table.Column<string>(nullable: true),
                    tell = table.Column<string>(nullable: true),
                    pass = table.Column<string>(nullable: true),
                    email = table.Column<string>(nullable: true),
                    DatecreEmp = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employee", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "authori");

            migrationBuilder.DropTable(
                name: "Employee");
        }
    }
}
