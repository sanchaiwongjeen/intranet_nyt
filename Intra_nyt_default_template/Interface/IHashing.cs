﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Intra_nyt.Interface
{
    public interface IHashing
    {
      String  HashPass(String pass);
    }
    public interface IsaveFile
    {
      Task<String> save(IFormFile filename,String path);
      //  String saveAction(IFormFile filename, String path);
    }
    public interface IBuidToken
    {
        String BuidToken(String user,String pass);
    }
    public interface IAuthen
    {
       Task<String> sentToken(String type);
    }
   


}
