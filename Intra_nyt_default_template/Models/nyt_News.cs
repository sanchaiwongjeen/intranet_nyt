﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace nyt_Netcore.Models
{
    [Table("nyt_News")]
    public class nyt_News
    {
        public int Id { get; set; }


        [StringLength(20, MinimumLength = 5)]
       // [StringLength(20)]
        [Required]
        public String title { get; set; }

        [StringLength(1000,MinimumLength =100)]
        [Required]
        public String detail { get; set; }

        //[RegularExpression("([0-9])", ErrorMessage = "Please enter valid Number")]
        [StringLength(100)]
        [Required]
        public String img { get; set; }


        [StringLength(100)]
       // [Required]
        public String attachfile { get; set; }

        [DataType(DataType.DateTime)]
       // [Required]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyy}")]
        public DateTime? createdate { get; set; }

        public String status { get; set; }

        [Required]
        public String department { get; set; }

        [NotMapped]
        public SelectList deplist { get; set; }
    }
}
