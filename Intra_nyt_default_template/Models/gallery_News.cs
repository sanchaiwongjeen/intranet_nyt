﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Intra_nyt_default_template.Models
{
    public class gallery_News
    {
        public int Id { get; set; }
        public String ref_Id { get; set; }
        public String type { get; set; }
        public String path { get; set; }
    }
}
