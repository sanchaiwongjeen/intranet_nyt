﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Intra_nyt_default_template.Models
{
   
    public class Employee
    {
        public int Id { get; set; }
        public String emp_Id { get; set; }
        public String dep_Id { get; set; }
        public String emp_name { get; set; }
        public String emp_sname { get; set; }
        public String emp_picture { get; set; }
        public String address { get; set; }
        public String tell { get; set; }
        public String pass { get; set; }
        public String email { get; set; }
        public DateTime DatecreEmp { get; set; }
    }
}
