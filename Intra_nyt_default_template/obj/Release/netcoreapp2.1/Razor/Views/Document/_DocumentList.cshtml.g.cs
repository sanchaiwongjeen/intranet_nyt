#pragma checksum "C:\Intranet_nyt]\Intra_nyt_default_template\Intra_nyt_default_template\Views\Document\_DocumentList.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "a6bd9495631b0f1c84b382860cca53d0f03da0f9"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Document__DocumentList), @"mvc.1.0.view", @"/Views/Document/_DocumentList.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Document/_DocumentList.cshtml", typeof(AspNetCore.Views_Document__DocumentList))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Intranet_nyt]\Intra_nyt_default_template\Intra_nyt_default_template\Views\_ViewImports.cshtml"
using Intra_nyt_default_template;

#line default
#line hidden
#line 2 "C:\Intranet_nyt]\Intra_nyt_default_template\Intra_nyt_default_template\Views\_ViewImports.cshtml"
using Intra_nyt_default_template.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"a6bd9495631b0f1c84b382860cca53d0f03da0f9", @"/Views/Document/_DocumentList.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"d4fcb90c290e1f4b84aec769a91c2677d7231ce9", @"/Views/_ViewImports.cshtml")]
    public class Views_Document__DocumentList : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<Intra_nyt_default_template.ModelsView.Document>>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(68, 588, true);
            WriteLiteral(@"<div class=""row"" style=""margin:10px 10px"">
    <div class=""col-sm-6"">
        <div class=""title-line"">
            <span>
                Document List
            </span>
        </div>
        <div class=""card"">
            <div class=""card-body"">
                <table class=""table table-hover"">
                    <thead>
                        <tr>
                            <th class=""border-top-0"">Title</th>
                            <th class=""border-top-0"">Date</th>
                        </tr>
                    </thead>
                    <tbody>
");
            EndContext();
#line 19 "C:\Intranet_nyt]\Intra_nyt_default_template\Intra_nyt_default_template\Views\Document\_DocumentList.cshtml"
                         foreach (var item in Model)
                        {

#line default
#line hidden
            BeginContext(737, 62, true);
            WriteLiteral("                        <tr>\r\n                            <td>");
            EndContext();
            BeginContext(800, 32, false);
#line 22 "C:\Intranet_nyt]\Intra_nyt_default_template\Intra_nyt_default_template\Views\Document\_DocumentList.cshtml"
                           Write(Html.DisplayFor(m => item.title));

#line default
#line hidden
            EndContext();
            BeginContext(832, 39, true);
            WriteLiteral("</td>\r\n                            <td>");
            EndContext();
            BeginContext(872, 37, false);
#line 23 "C:\Intranet_nyt]\Intra_nyt_default_template\Intra_nyt_default_template\Views\Document\_DocumentList.cshtml"
                           Write(Html.DisplayFor(m => item.createdate));

#line default
#line hidden
            EndContext();
            BeginContext(909, 38, true);
            WriteLiteral("</td>\r\n                        </tr>\r\n");
            EndContext();
#line 25 "C:\Intranet_nyt]\Intra_nyt_default_template\Intra_nyt_default_template\Views\Document\_DocumentList.cshtml"
                        }

#line default
#line hidden
            BeginContext(974, 112, true);
            WriteLiteral("                    </tbody>\r\n                </table>\r\n            </div>\r\n\r\n        </div>\r\n    </div>\r\n</div>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<Intra_nyt_default_template.ModelsView.Document>> Html { get; private set; }
    }
}
#pragma warning restore 1591
