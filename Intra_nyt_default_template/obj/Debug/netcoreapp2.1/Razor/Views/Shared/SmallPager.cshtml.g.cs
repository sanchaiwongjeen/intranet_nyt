#pragma checksum "F:\source\repos\Intra_nyt_default_template\Intra_nyt_default_template\Views\Shared\SmallPager.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "260823b870771128b401c32ee544447ab0ef7553"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Shared_SmallPager), @"mvc.1.0.view", @"/Views/Shared/SmallPager.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Shared/SmallPager.cshtml", typeof(AspNetCore.Views_Shared_SmallPager))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "F:\source\repos\Intra_nyt_default_template\Intra_nyt_default_template\Views\_ViewImports.cshtml"
using Intra_nyt_default_template;

#line default
#line hidden
#line 2 "F:\source\repos\Intra_nyt_default_template\Intra_nyt_default_template\Views\_ViewImports.cshtml"
using Intra_nyt_default_template.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"260823b870771128b401c32ee544447ab0ef7553", @"/Views/Shared/SmallPager.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"d4fcb90c290e1f4b84aec769a91c2677d7231ce9", @"/Views/_ViewImports.cshtml")]
    public class Views_Shared_SmallPager : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<ReflectionIT.Mvc.Paging.IPagingList>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(45, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 3 "F:\source\repos\Intra_nyt_default_template\Intra_nyt_default_template\Views\Shared\SmallPager.cshtml"
  
    var start = this.Model.StartPageIndex;
    var stop = this.Model.StopPageIndex;

#line default
#line hidden
            BeginContext(140, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 8 "F:\source\repos\Intra_nyt_default_template\Intra_nyt_default_template\Views\Shared\SmallPager.cshtml"
 if (this.Model.PageCount > 1) {

#line default
#line hidden
            BeginContext(176, 45, true);
            WriteLiteral("    <ul class=\"pagination pagination-sm\">\r\n\r\n");
            EndContext();
#line 11 "F:\source\repos\Intra_nyt_default_template\Intra_nyt_default_template\Views\Shared\SmallPager.cshtml"
         if (start > 1) {

#line default
#line hidden
            BeginContext(248, 36, true);
            WriteLiteral("            <li>\r\n                <a");
            EndContext();
            BeginWriteAttribute("href", " href=\"", 284, "\"", 347, 1);
#line 13 "F:\source\repos\Intra_nyt_default_template\Intra_nyt_default_template\Views\Shared\SmallPager.cshtml"
WriteAttributeValue("", 291, Url.Action(Model.Action, Model.GetRouteValueForPage(1)), 291, 56, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(348, 118, true);
            WriteLiteral(" aria-label=\"First\">\r\n                    <span aria-hidden=\"true\">1</span>\r\n                </a>\r\n            </li>\r\n");
            EndContext();
#line 17 "F:\source\repos\Intra_nyt_default_template\Intra_nyt_default_template\Views\Shared\SmallPager.cshtml"
        }

#line default
#line hidden
            BeginContext(477, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 19 "F:\source\repos\Intra_nyt_default_template\Intra_nyt_default_template\Views\Shared\SmallPager.cshtml"
         if (this.Model.PageIndex > 1) {

#line default
#line hidden
            BeginContext(521, 36, true);
            WriteLiteral("            <li>\r\n                <a");
            EndContext();
            BeginWriteAttribute("href", " href=\"", 557, "\"", 643, 1);
#line 21 "F:\source\repos\Intra_nyt_default_template\Intra_nyt_default_template\Views\Shared\SmallPager.cshtml"
WriteAttributeValue("", 564, Url.Action(Model.Action, Model.GetRouteValueForPage(this.Model.PageIndex - 1)), 564, 79, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(644, 127, true);
            WriteLiteral(" aria-label=\"Previous\">\r\n                    <span aria-hidden=\"true\">&laquo;</span>\r\n                </a>\r\n            </li>\r\n");
            EndContext();
#line 25 "F:\source\repos\Intra_nyt_default_template\Intra_nyt_default_template\Views\Shared\SmallPager.cshtml"
        }

#line default
#line hidden
            BeginContext(782, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 27 "F:\source\repos\Intra_nyt_default_template\Intra_nyt_default_template\Views\Shared\SmallPager.cshtml"
         for (int i = start; i <= stop; i++) {

#line default
#line hidden
            BeginContext(832, 15, true);
            WriteLiteral("            <li");
            EndContext();
            BeginWriteAttribute("class", " class=\"", 847, "\"", 898, 1);
#line 28 "F:\source\repos\Intra_nyt_default_template\Intra_nyt_default_template\Views\Shared\SmallPager.cshtml"
WriteAttributeValue("", 855, (Model.PageIndex == i) ? "active" : null, 855, 43, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(899, 19, true);
            WriteLiteral(">\r\n                ");
            EndContext();
            BeginContext(919, 74, false);
#line 29 "F:\source\repos\Intra_nyt_default_template\Intra_nyt_default_template\Views\Shared\SmallPager.cshtml"
           Write(Html.ActionLink(i.ToString(), Model.Action, Model.GetRouteValueForPage(i)));

#line default
#line hidden
            EndContext();
            BeginContext(993, 21, true);
            WriteLiteral("\r\n            </li>\r\n");
            EndContext();
#line 31 "F:\source\repos\Intra_nyt_default_template\Intra_nyt_default_template\Views\Shared\SmallPager.cshtml"
        }

#line default
#line hidden
            BeginContext(1025, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 33 "F:\source\repos\Intra_nyt_default_template\Intra_nyt_default_template\Views\Shared\SmallPager.cshtml"
         if (this.Model.PageIndex < this.Model.PageCount) {

#line default
#line hidden
            BeginContext(1088, 36, true);
            WriteLiteral("            <li>\r\n                <a");
            EndContext();
            BeginWriteAttribute("href", " href=\"", 1124, "\"", 1210, 1);
#line 35 "F:\source\repos\Intra_nyt_default_template\Intra_nyt_default_template\Views\Shared\SmallPager.cshtml"
WriteAttributeValue("", 1131, Url.Action(Model.Action, Model.GetRouteValueForPage(this.Model.PageIndex + 1)), 1131, 79, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(1211, 123, true);
            WriteLiteral(" aria-label=\"Next\">\r\n                    <span aria-hidden=\"true\">&raquo;</span>\r\n                </a>\r\n            </li>\r\n");
            EndContext();
#line 39 "F:\source\repos\Intra_nyt_default_template\Intra_nyt_default_template\Views\Shared\SmallPager.cshtml"
        }

#line default
#line hidden
            BeginContext(1345, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 41 "F:\source\repos\Intra_nyt_default_template\Intra_nyt_default_template\Views\Shared\SmallPager.cshtml"
         if (stop < this.Model.PageCount) {

#line default
#line hidden
            BeginContext(1392, 36, true);
            WriteLiteral("            <li>\r\n                <a");
            EndContext();
            BeginWriteAttribute("href", " href=\"", 1428, "\"", 1510, 1);
#line 43 "F:\source\repos\Intra_nyt_default_template\Intra_nyt_default_template\Views\Shared\SmallPager.cshtml"
WriteAttributeValue("", 1435, Url.Action(Model.Action, Model.GetRouteValueForPage(this.Model.PageCount)), 1435, 75, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(1511, 66, true);
            WriteLiteral(" aria-label=\"Last\">\r\n                    <span aria-hidden=\"true\">");
            EndContext();
            BeginContext(1578, 20, false);
#line 44 "F:\source\repos\Intra_nyt_default_template\Intra_nyt_default_template\Views\Shared\SmallPager.cshtml"
                                        Write(this.Model.PageCount);

#line default
#line hidden
            EndContext();
            BeginContext(1598, 50, true);
            WriteLiteral("</span>\r\n                </a>\r\n            </li>\r\n");
            EndContext();
#line 47 "F:\source\repos\Intra_nyt_default_template\Intra_nyt_default_template\Views\Shared\SmallPager.cshtml"
        }

#line default
#line hidden
            BeginContext(1659, 13, true);
            WriteLiteral("\r\n    </ul>\r\n");
            EndContext();
#line 50 "F:\source\repos\Intra_nyt_default_template\Intra_nyt_default_template\Views\Shared\SmallPager.cshtml"
}

#line default
#line hidden
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<ReflectionIT.Mvc.Paging.IPagingList> Html { get; private set; }
    }
}
#pragma warning restore 1591
