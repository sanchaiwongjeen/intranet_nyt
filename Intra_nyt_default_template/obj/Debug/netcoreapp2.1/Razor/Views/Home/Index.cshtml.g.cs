#pragma checksum "F:\source\repos\Intra_nyt_default_template\Intra_nyt_default_template\Views\Home\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "077f83d17c3d6551ed268092469eb9ee5aa3a713"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Index), @"mvc.1.0.view", @"/Views/Home/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Home/Index.cshtml", typeof(AspNetCore.Views_Home_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "F:\source\repos\Intra_nyt_default_template\Intra_nyt_default_template\Views\_ViewImports.cshtml"
using Intra_nyt_default_template;

#line default
#line hidden
#line 2 "F:\source\repos\Intra_nyt_default_template\Intra_nyt_default_template\Views\_ViewImports.cshtml"
using Intra_nyt_default_template.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"077f83d17c3d6551ed268092469eb9ee5aa3a713", @"/Views/Home/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"d4fcb90c290e1f4b84aec769a91c2677d7231ce9", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "nyt_News", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Details", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 1 "F:\source\repos\Intra_nyt_default_template\Intra_nyt_default_template\Views\Home\Index.cshtml"
  
    ViewData["Title"] = "Home Page";

#line default
#line hidden
            BeginContext(45, 334, true);
            WriteLiteral(@"
<div class=""row sec"">
    <div class=""col-md-9"" style=""margin-left:-10px"">
        <div id=""myCarousel"" class=""carousel slide"" data-ride=""carousel"" data-interval=""6000"">

            <div class=""carousel-outer"">
                <div class=""carousel-inner"" role=""listbox"">
                    <ol class=""carousel-indicators"">
");
            EndContext();
#line 12 "F:\source\repos\Intra_nyt_default_template\Intra_nyt_default_template\Views\Home\Index.cshtml"
                         for (int i = 0; i < ViewBag.dataimg.Count; i++)
                        {
                            String cStr = "";
                            if (i < 1)
                            {
                                cStr = "active";
                            }
                            else
                            {
                                cStr = "";
                            }

#line default
#line hidden
            BeginContext(819, 73, true);
            WriteLiteral("                            <li data-target=\"#myCarousel\" data-slide-to=\"");
            EndContext();
            BeginContext(893, 1, false);
#line 23 "F:\source\repos\Intra_nyt_default_template\Intra_nyt_default_template\Views\Home\Index.cshtml"
                                                                    Write(i);

#line default
#line hidden
            EndContext();
            BeginContext(894, 1, true);
            WriteLiteral("\"");
            EndContext();
            BeginWriteAttribute("class", " class=\"", 895, "\"", 908, 1);
#line 23 "F:\source\repos\Intra_nyt_default_template\Intra_nyt_default_template\Views\Home\Index.cshtml"
WriteAttributeValue("", 903, cStr, 903, 5, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(909, 8, true);
            WriteLiteral("></li>\r\n");
            EndContext();
#line 24 "F:\source\repos\Intra_nyt_default_template\Intra_nyt_default_template\Views\Home\Index.cshtml"
                        }

#line default
#line hidden
            BeginContext(944, 24, true);
            WriteLiteral("                        ");
            EndContext();
            BeginContext(1174, 29, true);
            WriteLiteral("\r\n                    </ol>\r\n");
            EndContext();
#line 29 "F:\source\repos\Intra_nyt_default_template\Intra_nyt_default_template\Views\Home\Index.cshtml"
                     for (int i = 0; i < ViewBag.dataimg.Count; i++)
                    {
                        String cDiv = "";
                        String slide = "";
                        String slideDetail = "";
                        slide = "slide" + i.ToString();
                        slideDetail = "slideDetail" + i.ToString();
                        if (i < 1)
                        {
                            cDiv = "item active";

                        }
                        else
                        {
                            cDiv = "item";
                        }

#line default
#line hidden
            BeginContext(1830, 28, true);
            WriteLiteral("                        <div");
            EndContext();
            BeginWriteAttribute("id", " id=\"", 1858, "\"", 1869, 1);
#line 45 "F:\source\repos\Intra_nyt_default_template\Intra_nyt_default_template\Views\Home\Index.cshtml"
WriteAttributeValue("", 1863, slide, 1863, 6, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginWriteAttribute("class", " class=\"", 1870, "\"", 1883, 1);
#line 45 "F:\source\repos\Intra_nyt_default_template\Intra_nyt_default_template\Views\Home\Index.cshtml"
WriteAttributeValue("", 1878, cDiv, 1878, 5, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(1884, 31, true);
            WriteLiteral(">\r\n                            ");
            EndContext();
            BeginContext(1915, 655, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "55a1c76c245d412c82de626ae5d72036", async() => {
                BeginContext(2003, 2, true);
                WriteLiteral("\r\n");
                EndContext();
#line 47 "F:\source\repos\Intra_nyt_default_template\Intra_nyt_default_template\Views\Home\Index.cshtml"
                                   String path = "http://192.168.2.11/information" + ViewBag.dataimg[i].img;

#line default
#line hidden
                BeginContext(2116, 36, true);
                WriteLiteral("                                <img");
                EndContext();
                BeginWriteAttribute("src", " src=\"", 2152, "\"", 2163, 1);
#line 48 "F:\source\repos\Intra_nyt_default_template\Intra_nyt_default_template\Views\Home\Index.cshtml"
WriteAttributeValue("", 2158, path, 2158, 5, false);

#line default
#line hidden
                EndWriteAttribute();
                BeginContext(2164, 180, true);
                WriteLiteral(" alt=\"ASP.NET\" class=\"\" style=\"width:1500px;height:350px\" />\r\n                                <div class=\"carousel-caption\" role=\"option\">\r\n\r\n                                    <p");
                EndContext();
                BeginWriteAttribute("id", " id=\"", 2344, "\"", 2361, 1);
#line 51 "F:\source\repos\Intra_nyt_default_template\Intra_nyt_default_template\Views\Home\Index.cshtml"
WriteAttributeValue("", 2349, slideDetail, 2349, 12, false);

#line default
#line hidden
                EndWriteAttribute();
                BeginContext(2362, 64, true);
                WriteLiteral(" style=\"display:none\">\r\n                                        ");
                EndContext();
                BeginContext(2427, 25, false);
#line 52 "F:\source\repos\Intra_nyt_default_template\Intra_nyt_default_template\Views\Home\Index.cshtml"
                                   Write(ViewBag.dataimg[i].detail);

#line default
#line hidden
                EndContext();
                BeginContext(2452, 114, true);
                WriteLiteral("\r\n\r\n                                    </p>\r\n                                </div>\r\n                            ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 46 "F:\source\repos\Intra_nyt_default_template\Intra_nyt_default_template\Views\Home\Index.cshtml"
                                                                                WriteLiteral(ViewBag.dataimg[i].Id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(2570, 36, true);
            WriteLiteral("\r\n\r\n                        </div>\r\n");
            EndContext();
#line 59 "F:\source\repos\Intra_nyt_default_template\Intra_nyt_default_template\Views\Home\Index.cshtml"
                    }

#line default
#line hidden
            BeginContext(2629, 866, true);
            WriteLiteral(@"
                  
                </div>
                <a class=""left carousel-control"" href=""#myCarousel"" role=""button"" data-slide=""prev"">
                    <span class=""glyphicon glyphicon-chevron-left"" aria-hidden=""true""></span>
                    <span class=""sr-only"">Previous</span>
                </a>
                <a class=""right carousel-control"" href=""#myCarousel"" role=""button"" data-slide=""next"">
                    <span class=""glyphicon glyphicon-chevron-right"" aria-hidden=""true""></span>
                    <span class=""sr-only"">Next</span>
                </a>
            </div>


        </div>
       
    </div>
    <div class=""col-md-3"">
        <div style=""margin-top:-20px"">
            <div class=""title-line"">
                <span>
                    ข่าวใหม่
                </span>
            </div>
");
            EndContext();
            BeginContext(3842, 234, true);
            WriteLiteral("            <p id=\"cDetail\"></p>\r\n        </div>\r\n       \r\n       \r\n    </div>\r\n</div>\r\n\r\n<div class=\"\">\r\n    <div class=\"title-line\">\r\n        <span>\r\n            ประชาสัมพันธ์\r\n        </span>\r\n    </div>\r\n\r\n    <div class=\"row\" >\r\n");
            EndContext();
#line 108 "F:\source\repos\Intra_nyt_default_template\Intra_nyt_default_template\Views\Home\Index.cshtml"
         foreach (var item in ViewBag.datanormal)
        {

#line default
#line hidden
            BeginContext(4439, 8, true);
            WriteLiteral("        ");
            EndContext();
            BeginContext(4447, 685, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "fde3d181e2bc4255b7fdaeca99bb2599", async() => {
                BeginContext(4521, 97, true);
                WriteLiteral("\r\n            <div class=\"col-md-4\" style=\"margin-top:3px\">\r\n                <div class=\"card\">\r\n");
                EndContext();
#line 113 "F:\source\repos\Intra_nyt_default_template\Intra_nyt_default_template\Views\Home\Index.cshtml"
                       String path = "http://192.168.2.11/information" + item.img;

#line default
#line hidden
                BeginContext(4703, 24, true);
                WriteLiteral("                    <img");
                EndContext();
                BeginWriteAttribute("src", " src=\"", 4727, "\"", 4738, 1);
#line 114 "F:\source\repos\Intra_nyt_default_template\Intra_nyt_default_template\Views\Home\Index.cshtml"
WriteAttributeValue("", 4733, path, 4733, 5, false);

#line default
#line hidden
                EndWriteAttribute();
                BeginContext(4739, 160, true);
                WriteLiteral(" style=\"width:357px\" class=\"zoom\" />\r\n                    <div class=\"card-body\">\r\n                        <p class=\"card-text\">\r\n\r\n                            ");
                EndContext();
                BeginContext(4900, 40, false);
#line 118 "F:\source\repos\Intra_nyt_default_template\Intra_nyt_default_template\Views\Home\Index.cshtml"
                       Write(item.detail.ToString().Substring(0, 100));

#line default
#line hidden
                EndContext();
                BeginContext(4940, 90, true);
                WriteLiteral("..\r\n                        </p>\r\n                        <hr />\r\n                        ");
                EndContext();
                BeginContext(5031, 15, false);
#line 121 "F:\source\repos\Intra_nyt_default_template\Intra_nyt_default_template\Views\Home\Index.cshtml"
                   Write(item.createdate);

#line default
#line hidden
                EndContext();
                BeginContext(5046, 82, true);
                WriteLiteral("\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 110 "F:\source\repos\Intra_nyt_default_template\Intra_nyt_default_template\Views\Home\Index.cshtml"
                                                            WriteLiteral(item.Id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(5132, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 126 "F:\source\repos\Intra_nyt_default_template\Intra_nyt_default_template\Views\Home\Index.cshtml"
          
        }

#line default
#line hidden
            BeginContext(5157, 29, true);
            WriteLiteral("       \r\n    </div>\r\n</div>\r\n");
            EndContext();
            DefineSection("Scripts", async() => {
                BeginContext(5203, 1104, true);
                WriteLiteral(@"

    <script>
        $(document).ready(function () {
            $(""#home"").addClass(""active"");
            var s = document.getElementById(""slideDetail0"").innerText;
            if (s.length > 240) {
                s = s.substring(0, 240);
            }
            $(""#cDetail"").text(s);
           // alert(s);
            $('#myCarousel').on('slid.bs.carousel', function () {
                for (i = 0; i <= 3; i++) {
                   
                    if ($(""#slide"" + i).hasClass(""active"")) {
                        
                        s = document.getElementById(""slideDetail"" + i).innerText;
                       // alert(s.length);
                        if (s.length > 240) {
                            s = s.substring(0,240);
                        }
                        $(""#cDetail"").html(s);
                    }
                }
               
            })

        });
    </script>
    <script type=""text/javascript"">
        function addDetail(titl");
                WriteLiteral("e, detail) {\r\n            $(\"#cDetail\").html(title);\r\n        }\r\n    </script>\r\n");
                EndContext();
            }
            );
            BeginContext(6310, 8, true);
            WriteLiteral("\r\n\r\n\r\n\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
