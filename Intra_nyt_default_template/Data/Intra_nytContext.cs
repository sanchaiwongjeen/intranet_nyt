﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using nyt_Netcore.Models;
using Intra_nyt_default_template.ModelsView;
namespace Intra_nyt_default_template.Models
{
    public class Intra_nytContext : DbContext
    {
        public Intra_nytContext (DbContextOptions<Intra_nytContext> options)
            : base(options)
        {
        }

        public DbSet<nyt_News> nyt_News { get; set; }
        public DbSet<department> department { get; set; }
        public DbSet<gallery_News> gallery_News { get; set; }
        public DbSet<Employee> Employee { get; set; }
        public DbSet<authori> authori { get; set; }
        public DbSet<Document> documents { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<nyt_News>(e =>
            {
                e.ToTable("nyt_News");
                e.HasKey(k => k.Id);
            });
            modelBuilder.Entity<department>(e =>
            {
                e.ToTable("department");
                e.HasKey(k => k.Id);
            });
            modelBuilder.Entity<gallery_News>(e =>
            {
                e.ToTable("gallery_News");
                e.HasKey(k => k.Id);
            });
            modelBuilder.Entity<Employee>(e =>
            {
                e.ToTable("Employee");
                e.HasKey(k => k.Id);
            });
            modelBuilder.Entity<authori>(e =>
            {
                e.ToTable("authori");
                e.HasKey(k => k.Id);
            });
            modelBuilder.Entity<Document>(e =>
            {
                e.ToTable("document");
                e.HasKey(k => k.Id);
            });
        }
    }
}
